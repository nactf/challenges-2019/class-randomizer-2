CC=gcc
CFLAGS=-lsodium -lm

.PHONY: default
default: class-randomizer-2

%: %.c
	$(CC) -o $@ $< $(CFLAGS)
